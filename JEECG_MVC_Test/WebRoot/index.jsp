<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
  	<link rel="stylesheet" href="jquery-easyui-1.3.4/themes/default/easyui.css" type="text/css"></link>
  	<script type="text/javascript" src="<%=basePath %>jquery-1.9.1.js"></script>
  	<script type="text/javascript" src="<%=basePath %>jquery-easyui-1.3.4/jquery.easyui.min.js"></script>
  	<script type="text/javascript" src="<%=basePath %>jquery-easyui-1.3.4/locale/easyui-lang-zh_CN.js"></script>
  	<script type="text/javascript">
  		function login(){
  			if($("#loginForm").form("validate")){
  				$("#loginBtn").linkbutton("disable");
  				$.post("<%=basePath%>login.do",$("#loginForm").serialize(),function(data){
  					$.messager.alert("返回值",data);
  				},"text");
  			}
  		}
  		$(function(){
  			$("#div_dialog").dialog({
  				width:350,
  				height:200,
  				model:true,
  				closable:false,
  				buttons:[
  					{
  						id:'loginBtn',
	  					text:'登录',
	  					iconCIs:'icon-ok',
	  					handler:function(){
							login();
	  					}
  					},
  					{
  						id:'regBtn',
	  					text:'注册',
	  					iconCIs:'icon-ok',
	  					handler:function(){
	  						$.messager.alert('提示','Sign Up');
	  					}
  					},
  				]
  			});
  		});
  	</script>
  	
  </head>
  
  <body>
  	<div id="div_dialog" title="登录">
  		<div title="用户输入模式" style="overflow: hidden; padding: 10px;font-size: 12px;">
			<form method="post" class="form" id="loginForm">
				<table class="table" style="width: 100%; height: 74%;font-size: 14px;font-family: 'Microsoft YaHei';">
					<tr>
						<th width="50">登录名</th>
						<td><input type="hidden" name="key" value="com.mc.mvc.form.LoginForm"/><input name="userName" class="easyui-validatebox" data-options="required:true" value="" style="width: 210px;" /></td>
					</tr>
					<tr>
						<th>密码</th>
						<td><input name="passWord" type="password" class="easyui-validatebox" data-options="required:true" value="" style="width: 210px;" /></td>
					</tr>
				</table>
			</form>
		</div>
	</div>
  </body>
</html>

package com.mc.mvc.form;

public class LoginForm implements Comparable{
	private String userName;
	private String passWord;
	
	public LoginForm() {
	}
	

	public LoginForm(String userName, String passWord) {
		super();
		this.userName = userName;
		this.passWord = passWord;
	}


	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassWord() {
		return passWord;
	}

	public void setPassWord(String passWord) {
		this.passWord = passWord;
	}
	@Override
	public String toString() {
		return "userName="+this.userName+" passWord="+this.passWord;
	}


	public int compareTo(Object o) {
		return 0;
	}
}

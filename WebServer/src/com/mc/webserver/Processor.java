package com.mc.webserver;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.Socket;

/**
 * 处理Http请求 接收和返回等操作
 * 线程方式运行
 * Core Class
 * @author MC
 *
 */
public class Processor extends Thread{
	private Socket socket;
	private InputStream in;
	private PrintStream out;
	private final static String WEB_ROOT = "d:\\java_web";
	
	public Processor(Socket socket) {
		this.socket = socket;
		try {
			in = socket.getInputStream();
			out = new PrintStream(socket.getOutputStream()); 
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * 线程启动方法
	 */
	public void run() {
		String fileName = parse(in); //访问的资源文件
		sendFile(fileName);
	}
	
	/**
	 * 解析输入流，输入流从socket来
	 * @param in
	 * @return
	 */
	public String parse(InputStream in){
		BufferedReader br = new BufferedReader(new InputStreamReader(in));
		String fileName = null;
		try {
			String httpMessage = br.readLine();
			String[] content = httpMessage.split(" ");
			
			if(content.length!=3){
				this.sendErrorMessage(400, "Client query erro");
				return null;
			}
			
			System.out.println("code:"+content[0]+",fileName:"+content[1]+",http version:"+content[2]);
			fileName = content[1];
		} catch (IOException e) {
			e.printStackTrace();
		}
		return fileName;
	}
	
	public void sendErrorMessage(int errorCode,String errorMessage){
		out.println("HTTP/1.0 "+errorCode+" "+errorMessage);
		out.println("content-type: text/html");
		out.println();
		out.println("<html>");
		out.println("<title>Error Message</title>");
		out.println("<body>");
		out.println("<h1>出错啦！ErrorCode:"+errorCode+",ErrorMessage:"+errorMessage+"</h1>");
		out.println("</body>");
		out.println("</html>");
		out.close();
	}
	
	public void sendFile(String fileName){
		File file = new File(Processor.WEB_ROOT+fileName);
		if(!file.exists()){
			this.sendErrorMessage(404, "File Not Found");
			return;
			
		}
		try {
			InputStream in = new FileInputStream(file);
			byte[] content =  new byte[(int) file.length()];
			in.read(content);
			out.println("HTTP/1.0 200 queryfile");
			out.println("content-length:"+content.length);
			out.println();
			out.write(content);
			out.close();
			in.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
